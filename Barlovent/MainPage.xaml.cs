﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using System.IO.Compression;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using Windows.UI.ViewManagement;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Barlovent
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static List<string> _sharedStrings;
        private ObservableCollection<ItemData> _items = new ObservableCollection<ItemData>();

        public ObservableCollection<ItemData> Items { get { return this._items; } }

        public MainPage()
        {
            this.InitializeComponent();

            ApplicationView.PreferredLaunchViewSize = new Size(700, 650);
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().Title = "Software Productos";
        }

        private async void Button_ClickAsync(object sender, RoutedEventArgs e)
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker();

            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".xls");
            picker.FileTypeFilter.Add(".xlsx");
            
            StorageFile file = await picker.PickSingleFileAsync();
            
            if (file != null)
            {
                ZipArchive z = new ZipArchive(file.OpenStreamForReadAsync().Result);

                var worksheet = z.GetEntry("xl/worksheets/sheet1.xml");
                var sharedString = z.GetEntry("xl/sharedStrings.xml");

                _sharedStrings = new List<string>();
                using (var sr = sharedString.Open())
                {
                    XDocument xdoc = XDocument.Load(sr);
                    _sharedStrings =
                        (
                        from xe in xdoc.Root.Elements()
                        select xe.Elements().First().Value
                        ).ToList();
                }

                using (var sr = worksheet.Open())
                {
                    XDocument xdoc = XDocument.Load(sr);
                    XNamespace xmlns = "http://schemas.openxmlformats.org/spreadsheetml/2006/main";
                    XElement sheetData = xdoc.Root.Element(xmlns + "sheetData");

                    foreach (var row in sheetData.Elements())
                    {
                        var rowItem = new ItemData();
                        rowItem.Proveedor = _sharedStrings[Convert.ToInt32(row.Elements().ElementAt(0).Value)];
                        rowItem.Descripcion = _sharedStrings[Convert.ToInt32(row.Elements().ElementAt(1).Value)];
                        rowItem.Precio = row.Elements().ElementAt(2).Value;

                        _items.Add(rowItem);
                    }
                }

                //this.textBlock.Text = "Picked photo: " + file.Name;
            }
            else
            {
                //this.textBlock.Text = "Operation cancelled.";
            }
        }

        private void Button1_ClickAsync(object sender, RoutedEventArgs e)
        {

        }

        private void Button2_ClickAsync(object sender, RoutedEventArgs e)
        {

        }

        private void Button3_ClickAsync(object sender, RoutedEventArgs e)
        {

        }
    }
}
