﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Barlovent
{
    public class ItemData
    {
        public string Proveedor { get; set; }
        public string Descripcion { get; set; }
        public string Precio { get; set; }
    }
}
